The node.js script fetches the top stories from Hacker News, then generates an updated HTML page. When open, the page periodically updates story scores and checks for new stories without a page reload.

[hotonhn.com](https://hotonhn.com)
