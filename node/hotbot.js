/**
 * This node.js script fetches the top stories from Hacker News and then:
 * 1. generates updated website files
 * 2. tweets any hot stories that haven't previously been tweeted
 * 3. sends email notifications to subscribed addresses
 *
 * A hot story is determined by the number of votes it has received on HN,
 * i.e. the value of the 'score' property in the JSON fetched from the API.
 *
 * The script exits after running and is intended to be run at regular intervals.
 *
 * Dependencies:
 *   Hacker News API v0 (https://hacker-news.firebaseio.com/v0/)
 *   node-fetch - node.js implementation of browser fetch API (available from npm)
 *   throat - limits concurrency of async promise-returning functions (available from npm)
 *   ttytter - command line tweet program (www.floodgap.com/software/ttytter/)
 *   Twitter account that will be used to tweet hot stories
 *   Writable directories for the log and website files
 * 
 * NOTE: Tweeting has been disabled because Twitter has locked down their APIs.
 */

/* globals process */
'use strict';

let db, exec, transport;  // Required later only if needed

const fetch = require('node-fetch'),
         fs = require('fs'),
     throat = require('throat'),

     hotDir = '/home/sean/hot/';

const files = {
    templates: {
        index:    hotDir + 'node/index.html',
        rss:      hotDir + 'node/rss.xml'
    },
    json: {
        more:     hotDir + 'www/more.json',
        stories:  hotDir + 'www/stories.json'
    },
    index:        hotDir + 'www/index.html',
    rss:          hotDir + 'www/rss.xml',
    log:          hotDir + 'log/hotbot.log',
    dkimKey:      hotDir + 'node/dkim.key',
    // ttytterkey:   hotDir + 'ttytterkey',
    // ttytter:      '/home/sean/ttytter.pl',
    sendmail:     '/usr/lib/sm.bin/sendmail'
};


const hb = {
    numStories:   30,      // Number of top stories to check on Hacker News
    numVotes:     300,     // Minimum number of votes that indicates a hot story
    // tweetQueue:   [],      // Array of stories to be tweeted
    startDate:    new Date(),

    apiUrl:       'https://hacker-news.firebaseio.com/v0/',
    dbConnect:    'hot-user:bp1p3st0rag3m@localhost/HotOnHN',
    insertPoint:  '<!-- Hot Stories -->',

    fetchOptions: {
        method:   'GET',   // GET should be default but set it anyway
        timeout:  30000,   // In milliseconds (0 to disable, OS limit applies)
        size:     30000    // Maximum response body size in bytes (0 to disable)
    },


    /**
     * Outputs the passed string to log file and console.
     * Default prefix is a newline character.
     */

    log(string = '', prefix = '\n') {
        string.length && ['.', '['].includes(string.charAt(0)) && (prefix = '');
        fs.appendFileSync(files.log, prefix + string);
        // Don't use console.log in production as it outputs to syslog
        //console.log(string);
    },


    /**
     * Logs then throws the passed error string.
     */

    handleError(string) {
        //hb.log(`handleError(${string})`);
        hb.log(string);
        throw string;
    },


    /**
     * Synchronously reads the content of the specified file.
     * Wraps fs.readFileSync to include content and error handling.
     * @return {string} File content
     */

    readFile(file) {
        //hb.log(`readFile(${file})`);
        let content = fs.readFileSync(file);
        if (content) {
            content = content.toString();
        } else {
            hb.handleError(`Failed to read ${file}`);
        }
        return content;
    },


    /**
     * Asynchronously writes content to the specified file.
     * Wraps fs.writeFile to include error handling.
     */

    writeFile(file, content) {
        //hb.log(`writeFile(${file})`);
        fs.writeFile(file, content, err => {
            err && hb.handleError(err);
        });
    },


    /**
     * Extracts the hostname from the passed url.
     * @param {string} url
     * @param {boolean} [www]
     * @return {string} Extracted hostname
     */

    getHostName(url, www = false) {
        //hb.log(`getHostName(${url}, ${www})`);
        const match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
        let hostName = '';
        if (match && match.length > 2 && typeof match[2] === 'string' && match[2].length) {
            hostName = match[2];
            www && (hostName = match[1] + hostName);
        }
        return hostName;
    },


    /**
     * Checks if the passed string is a valid Unix timestamp.
     * @param {number} timestamp
     * @return {boolean} true if url is valid, otherwise false
     */

    isTimestamp(timestamp) {
        //hb.log(`isTimestamp(${timestamp})`);
        return (typeof timestamp === 'number' &&
            !isNaN(timestamp) && timestamp > 1000000000);
    },


    /**
     * Formats the passed date as month and day (e.g. 'Jan 1').
     * @param {date|number} date - Date object or Unix timestamp
     * @return {string} Formatted date
     */

    formatDate(date = new Date()) {
        //hb.log(`formatDate(${date})`);
        hb.isTimestamp(date) && (date = new Date(date));
        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return months[date.getMonth()] + ' ' + date.getDate();
    },


    /**
     * Formats the passed date as 24 hour time (e.g. '12:00').
     * @param {date|number} date - Date object or Unix timestamp
     * @return {string} Formatted time
     */

    formatTime(date = new Date()) {
        //hb.log(`formatTime(${date})`);
        hb.isTimestamp(date) && (date = new Date(date));
        const hours = ('0' + date.getHours()).slice(-2),
            minutes = ('0' + date.getMinutes()).slice(-2);
        return `${hours}:${minutes}`;
    },


    /**
     * Checks if the passed string is a valid browser url.
     * @param {string} url
     * @return {boolean} true if url is valid, otherwise false
     */

    isValidUrl(url) {
        //hb.log(`isValidUrl(${url})`);
        const pattern = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;
        return (typeof url === 'string' && !!url.match(pattern));
    },


    /**
     * Checks the passed object has valid and necessary data.
     * This checks the keys specified by the Hacker News API v0.
     * @param {object} story
     * @return {boolean} true if data is valid, otherwise false
     */

    isValidStory(story) {
        //hb.log(`isValidStory(${story})`);
        return (story.dead !== true && story.deleted !== true && story.type === 'story' &&
            typeof story.id === 'number' && !isNaN(story.id) &&
            typeof story.descendants === 'number' && !isNaN(story.descendants) &&
            typeof story.score === 'number' && !isNaN(story.score) &&
            typeof story.title === 'string' && story.title.length &&
            hb.isTimestamp(story.time) && hb.isValidUrl(story.url));
    },


    /**
     * Creates an RSS XML story item.
     * @param {object} story
     * @return {string} XML
     */

    createRssItem(story) {
        //hb.log(`createRssItem(${story})`);
        return `
        <item>
            <title>${story.title}</title>
            <link>${story.url}</link>
            <comments>https://news.ycombinator.com/item?id=${story.id}</comments>
            <guid isPermaLink="false">${story.id}</guid>
            <pubDate>${new Date(story.time)}</pubDate>
        </item>
`;
    },


    /**
     * Creates an HTML story item.
     * @param {object} story
     * @return {string} HTML
     */

    createHtml(story) {
        //hb.log(`createHtml(${story})`);
        const { id, score, time, title, url, host, comments } = story,
            hostClasses = host.length >= 30 ? 'host long' : 'host';
        return `
            <a class="story" id="${id}" href="${url}" rel="noopener" target="_blank">
                <span class="score">${score}</span>
                <span class="title">${title}</span>
                <div class="meta">
                    <span class="${hostClasses}">
                        <img src="https://www.google.com/s2/favicons?domain=${host}">${host}
                    </span>
                    <span class="comments" onclick="return hot.openComments('${id}')">${comments}</span>
                    <span class="date" data-timestamp="${time}">${hb.formatDate(time)}</span>
                </div>
            </a>
`;
    },


    /**
     * Updates story properties and removes extraneous properties.
     * @param {object} story
     * @param {string} [timestamp]
     * @return {object} story
     */

    formatStory(story, timestamp = Date.now()) {
        //hb.log(`formatStory(${story}, ${timestamp})`);
        story.host = hb.getHostName(story.url);
        story.comments = story.descendants;
        story.time = timestamp;
        delete story.by;
        delete story.descendants;
        delete story.kids;
        delete story.type;
        return story;
    },


    /**
     * Fetches story data for ID from the Hacker News API then validates it.
     * @param {object} story
     * @return {object} story
     */

    updateStory(story) {
        //hb.log(`updateStory(${story.id})`);
        return fetch(`${hb.apiUrl}item/${story.id}.json`, hb.fetchOptions)
            .then(response => response.json())
            .then(data => {
                return hb.isValidStory(data) ? hb.formatStory(data, story.time) : story;
            }).catch(err => {
                //hb.handleError(err);
                hb.log('[error]');
                hb.log(err);
                return story;
            });
    },


    /**
     * Fetches data for ID from the Hacker News API then validates it and tweets story.
     * @param {number} id
     * @return {object} story
     */

    processNewStory(id) {
        //hb.log(`processNewStory(${id})`);
        return fetch(`${hb.apiUrl}item/${id}.json`, hb.fetchOptions)
            .then(response => response.json())
            .then(story => {
                if (hb.isValidStory(story) && story.score >= hb.numVotes) {
                    story = hb.formatStory(story);
                    //hb.log(`${id}: ${story.title} (${story.score})`);
                    // hb.tweetQueue.push(story);
                    return story;
                }
            }).catch(error => {
                //hb.handleError(error);
                hb.log(error);
                return undefined;
            });
    },


    /**
     * Trims the passed title string if necessary.
     * @param {string} title
     * @return {string} title
     */

    trimTitle(title) {
        //hb.log(`trimTitle(${title})`);
        return title.length > 115 ? title.trim(0, 112) + '...' : title;
    },


    /**
     * Tweets passed story using ttytter.
     * ttytter must be setup to use an account and keyfile with the Twitter API.
     * @param {object} story
     */

    // tweet(story) {
    //     //hb.log(`tweet(${story.id})`);
    //     const text = `${hb.trimTitle(story.title)}\n${story.url}`;
    //     return new Promise((resolve, reject) => {
    //         exec(`${files.ttytter} -keyf=${files.ttytterkey} -status="${text}"`, (error, stdout, stderr) => {
    //             if (error) {
    //                 //hb.handleError(error);
    //                 hb.log(error);
    //                 reject(error);
    //             } else {
    //                 resolve(story);
    //             }
    //         });
    //     });
    // },


    /**
     * Sends email notification of a hot story.
     */

    sendEmail({ to, threshold, story }) {
        //hb.log(`sendEmail(${to}, ${threshold})`);
        const from = 'notify@hotonhn.com',
           subject = 'Hot on HN: ' + story.title,
              html =

`<!doctype html>
<html>
    <body style="font-family: Helvetica, sans-serif; text-align: center;">
        <p style="color: #333333;">A story submitted to Hacker News has gained ${story.score} upvotes:</p>
        <h1 style="font-size: 1.25em; line-height: 1.3; margin-top: 2em; margin-bottom: 0;">
            <a style="color: #111111; text-decoration: none" href="${story.url}">${story.title}</a>
        </h1>
        <p style="color: #777777; font-size: 0.8em; font-style: italic; margin-top: 1em; margin-bottom: 1em;">${story.host}</p>
        <p style="font-size: 1em; margin-top: 2em; margin-bottom: 1em;">
            <a style="color: #ee6000; text-decoration: none;" href="https://news.ycombinator.com/item?id=${story.id}">${story.comments} Comments</a>
        </p>
        <ul style="color: #888888; font-size: 0.8em; margin-top: 4em; padding-left: 0; list-style-type: none;">
            <li style="display: inline; margin-left: 0; margin-right: 0;">
                <a style="color: #555555; text-decoration: none;" href="https://hotonhn.com">Hot on HN</a>
            </li>
            <li style="display: inline; margin-left: 1em; margin-right: 1em;">|</li>
            <li style="display: inline; margin-left: 0; margin-right: 0;">
                <a style="color: #555555; text-decoration: none;" href="https://hotonhn.com/subscribe/?email=${to}&threshold=${threshold}">Change Threshold</a>
            </li>
            <li style="display: inline; margin-left: 1em; margin-right: 1em;">|</li>
            <li style="display: inline; margin-left: 0; margin-right: 0;">
                <a style="color: #555555; text-decoration: none;" href="https://hotonhn.com/unsub/?email=${to}">Unsubscribe</a>
            </li>
        </ul>
    </body>
</html>`;

        transport.sendMail({ from, to, subject, html }, err => {
            if (err) {
                hb.log('[error]');
                hb.log('Failed to send email to ' + to);
            }
        });
    },


    saveThresholds(stories) {
        //console.log(`saveThresholds(${stories})`);
        hb.log('Saving threshold stories to database...');
        let count = stories.length;
        stories.forEach(({ id, score }) => {
            hb.log('id: ' + id + ', score: ' + score);
            db.thresholds.save({ _id: id, score }, err => {
                if (err) {
                    hb.log('[error]');
                    hb.log(`Failed to save ${id} to database!`);
                }
                if (--count === 0) {
                    hb.log('[done]');
                    db.close(() => { db = null; });
                }
            });
        });
    },


    /**
     * Writes JSON and markup files that are served as part of the site.
     * @param {array} stories
     */

    writeSiteFiles(stories) {
        //hb.log(`writeSiteFiles(${stories})`);
        hb.writeFile(files.json.stories, JSON.stringify(stories));
        hb.writeFile(files.rss, hb.rss.replace(hb.insertPoint, stories.reduce((xml, story) => xml += hb.createRssItem(story), '')));
        hb.writeFile(files.index, hb.index
            .replace('<span id="updated"></span>', `<span id="updated" data-timestamp="${Date.now()}">Updated ${hb.formatTime()} UTC</span>`)
            .replace(hb.insertPoint, stories.reduce((html, story) => html += hb.createHtml(story), '')));
    },


    /**
     * Writes JSON and markup files that contain more (older) stories.
     * @param {array} stories
     */

    writeMoreFiles(stories) {
        //hb.log(`writeMoreFiles(${stories})`);
        hb.writeFile(files.json.more, JSON.stringify(stories));
        let i = 0;
        while (stories.length) {
            hb.writeFile(`${hotDir}www/more${++i}.html`, stories.slice(0, 30).reduce((html, story) => html += hb.createHtml(story), ''));
            stories = stories.slice(30);
        }
    }
};


process.on('unhandledRejection', hb.log);
hb.log('--------------------------------------------------------------------------------');
hb.log('Starting hotbot script on ' + hb.startDate.toString());
hb.log('Reading files...');
hb.index = hb.readFile(files.templates.index);
hb.rss = hb.readFile(files.templates.rss);
hb.stories = JSON.parse(hb.readFile(files.json.stories));
hb.more = JSON.parse(hb.readFile(files.json.more));
hb.log('[done]');
hb.log('Fetching list of top story IDs...');

fetch(hb.apiUrl + 'topstories.json')
    .catch(hb.handleError)
    .then(response => response.json())
    .then(topIds => {
        if (!Array.isArray(topIds)) {
            hb.log('[error]');
            hb.handleError('Corrupt response when fetching IDs!');
        }
        hb.log('[done]');
        hb.log('Processing list of IDs...');
        const prevIds = hb.stories.map(story => story.id).concat(hb.more.map(story => story.id));
        topIds = topIds.slice(0, hb.numStories).filter(id => !prevIds.includes(id));
        hb.log('[done]');
        //hb.log('New top stories: ' + topIds);
        hb.log('Fetching data for new top stories...');
        return Promise.all(topIds.map(throat(1, hb.processNewStory)));
    })
    .then(topStories => {
        hb.log('[done]');
        hb.topStories = topStories.filter(story => typeof story === 'object');
        hb.log('Fetching latest data for previous hot stories...');
        return Promise.all(hb.stories.map(throat(1, hb.updateStory)));
    })
    .then(stories => {
        hb.log('[done]');
        stories = hb.topStories.concat(stories.filter(story => typeof story === 'object'));
        hb.log('Writing files...');
        while (stories.length > 30) {
            hb.more.unshift(stories.pop());
        }
        hb.writeSiteFiles(stories);
        hb.writeMoreFiles(hb.more);
        return stories;
    })
    .then(stories => {
        hb.log('[done]');
        const fetchedStories = stories.filter(({ score }) => score >= 1000);
        if (fetchedStories.length) {
            hb.log(`Processing ${fetchedStories.length} stories over threshold 1000...`);
            const storiesById = {};
            fetchedStories.forEach(story => {
                storiesById[story.id] = story;
            });
            const mongo = require('mongojs');
            db = mongo(hb.dbConnect, ['thresholds', 'subscriptions']);
            db.thresholds.find({ _id: { $in: Object.keys(storiesById).map(score => parseInt(score)) } }, (err, prevStories) => {
                if (err || !prevStories) {
                    hb.log('[error]');
                    hb.log('Failed to find thresholds in database!');
                    db.close();
                } else {
                    hb.log('prevStories.length: ' + prevStories.length);
                    //hb.log('storiesById before filter: ' + JSON.stringify(storiesById));
                    const thresholdStories = prevStories.filter(story => {
                        const isIncreased = storiesById[story._id].score > story.score;
                        isIncreased || (delete storiesById[story._id]);
                        return isIncreased;
                    }).map(prevStory => {
                        const story = storiesById[prevStory._id];
                        story.prevScore = prevStory.score;
                        delete storiesById[prevStory._id];
                        return story;
                    });
                    //hb.log('storiesById after filter: ' + JSON.stringify(storiesById));
                    fetchedStories.forEach(story => {
                        storiesById[story.id] && thresholdStories.push(story);
                    });
                    hb.log('[done]');
                    hb.log('thresholdStories.length: ' + thresholdStories.length);
                    //hb.log('thresholdStories: ' + JSON.stringify(thresholdStories));
                    if (thresholdStories.length) {
                        hb.log(`Processing ${thresholdStories.length} with increased threshold...`);
                        db.subscriptions.find({}, (err, subs) => {
                            if (err || !subs) {
                                hb.log('[error]');
                                hb.log('Failed to find subscriptions in database!');
                                db.close(() => { db = null; });
                            } else {
                                hb.log('subs: ' + JSON.stringify(subs));
                                let emailQueue = [];
                                thresholdStories.forEach(story => {
                                    const filteredSubs = subs.filter(({ threshold }) => threshold <= story.score && threshold > (story.prevScore || 0));
                                    emailQueue = emailQueue.concat(filteredSubs.map(({ _id: to, threshold }) => ({ to, threshold, story })));
                                });
                                hb.log('[done]');
                                hb.log('emailQueue.length: ' + emailQueue.length);
                                if (emailQueue.length) {
                                    hb.log('Sending queued emails...');
                                    const mailer = require('nodemailer');
                                    if (mailer) {
                                        transport = mailer.createTransport({
                                            sendmail: true,
                                            path: files.sendmail,
                                            dkim: {
                                                domainName: 'hotonhn.com',
                                                keySelector: 'dkim',
                                                privateKey: hb.readFile(files.dkimKey)
                                            }
                                        });
                                    }
                                    if (transport) {
                                        Promise.all(emailQueue.map(throat(1, hb.sendEmail))).then(() => {
                                            hb.log('[done]');
                                            hb.saveThresholds(thresholdStories);
                                        });
                                    } else {
                                        hb.log('[error]');
                                        hb.log('Failed to initialize mail transport!');
                                    }
                                } else {
                                    hb.log('Email queue is empty');
                                    hb.saveThresholds(thresholdStories);
                                }
                            }
                        });
                    } else {
                        db.close(() => { db = null; });
                    }
                }
            });
        }
    })
    // .then(() => {
    //     if (hb.tweetQueue.length) {
    //         hb.log('Tweeting queued stories...');
    //         exec = require('child_process').exec;
    //     }
    //     return Promise.all(hb.tweetQueue.map(throat(1, hb.tweet)));
    // })
    .then(() => {
        // hb.tweetQueue.length && hb.log('[done]');
        (function tryFinish(count = 0) {
            if (db && count < 300) {
                setTimeout(() => { tryFinish(++count); }, 1000);
            } else {
                db && db.close(() => { db = null; });
                const runningTime = ((new Date().getTime() - hb.startDate.getTime()) / 1000).toFixed(0);
                hb.log(`All tasks completed after ${runningTime} seconds`);
            }
        })();
    })
    .catch(hb.handleError);
