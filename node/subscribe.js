'use strict';

const mongo = require('mongojs'),
       http = require('http'),
        url = require('url'),
     mailer = require('nodemailer'),
         fs = require('fs'),

       port = 20402,
    divider = '--------------------------------------------------------------',
       from = 'notify@hotonhn.com',
 collection = 'subscriptions',
         db = mongo('hot-user:bp1p3st0rag3m@localhost/HotOnHN', [collection]),

  transport = mailer.createTransport({
        sendmail: true,
        path: '/usr/lib/sm.bin/sendmail',
        args: ['-f', from, '-C', '/etc/mail/submit.cf'],
        dkim: {
            domainName: 'hotonhn.com',
            keySelector: 'dkim',
            privateKey: fs.readFileSync('./dkim.key')
        }
    });


function validEmail(email) {
    const regex = /^(([^<>()\[\]\\.,;:\s@\"]+(\.[^<>()\[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
    return (typeof email === 'string' && regex.test(email)) ? email : false;
}

function validThreshold(threshold) {
    threshold = parseInt(threshold);
    return (threshold > 999 && threshold < 6001) ? threshold : false;
}

function sendSubEmail({ email: to, subject, threshold }) {
    const html = `<!doctype html>
<html>
    <body style="font-family: Helvetica, sans-serif; text-align: center;">
        <p style="color: #333333;">You'll receive an email notification when a story submitted to Hacker News gains ${threshold} upvotes.</p>
        <ul style="color: #888888; font-size: 0.8em; margin-top: 3em; padding-left: 0; list-style-type: none;">
            <li style="display: inline; margin-left: 0; margin-right: 0;">
                <a style="color: #555555; text-decoration: none;" href="https://hotonhn.com">Hot on HN</a>
            </li>
            <li style="display: inline; margin-left: 1em; margin-right: 1em;">|</li>
            <li style="display: inline; margin-left: 0; margin-right: 0;">
                <a style="color: #555555; text-decoration: none;" href="https://hotonhn.com/subscribe/?email=${to}&threshold=${threshold}&change=true">Change Threshold</a>
            </li>
            <li style="display: inline; margin-left: 1em; margin-right: 1em;">|</li>
            <li style="display: inline; margin-left: 0; margin-right: 0;">
                <a style="color: #555555; text-decoration: none;" href="https://hotonhn.com/unsub/?email=${to}">Unsubscribe</a>
            </li>
        </ul>
    </body>
</html>`;

    transport.sendMail({ from, to, subject, html }, err => {
        err && console.log('ERROR: Failed to send sub email to ' + to);
    });
}

function sendUnsubEmail({ email: to, threshold }) {
    const html = `<!doctype html>
<html>
    <body style="font-family: Helvetica, sans-serif; text-align: center;">
        <p style="color: #333333;">You'll no longer receive email notifications from Hot on HN.</p>
        <ul style="color: #888888; font-size: 0.8em; margin-top: 3em; padding-left: 0; list-style-type: none;">
            <li style="display: inline; margin-left: 0; margin-right: 0;">
                <a style="color: #555555; text-decoration: none;" href="https://hotonhn.com">Hot on HN</a>
            </li>
            <li style="display: inline; margin-left: 1em; margin-right: 1em;">|</li>
            <li style="display: inline; margin-left: 0; margin-right: 0;">
                <a style="color: #555555; text-decoration: none;" href="https://hotonhn.com/subscribe/?email=${to}&threshold=${threshold}">Resubscribe</a>
            </li>
        </ul>
    </body>
</html>`;

    transport.sendMail({ from, to, subject: 'Unsubscribed from Hot on HN', html }, err => {
        err && console.log('ERROR: Failed to send unsub email to ' + to);
    });
}

function sendResponse(res, code, text) {
    res.writeHead(code, {
        'access-control-allow-origin': 'https://hotonhn.com',
        'content-type': 'text/plain'
    });
    text && res.write(text);
    res.end('\n');
    console.log(`Sent response: ${code} ${http.STATUS_CODES[code]}${text ? ' (with text)' : ''}`);
}

function getClientIP(req) {
    const ip = (req.headers['x-forwarded-for'] || '').split(',')[0] || req.connection.remoteAddress;
    return ip.substring(ip.lastIndexOf(':') + 1, ip.length);
}


const server = http.createServer((req, res) => {
    console.log(divider);

    const reqUrl = url.parse(req.url, true),
          action = reqUrl.pathname.replace(/\//g, ''),
           email = validEmail(reqUrl.query.email);

    if (req.method === 'GET' && email) {
        if (action === 'sub') {
            const threshold = validThreshold(reqUrl.query.threshold);
            console.log(email + ' requests notifications at threshold ' + threshold);
            if (threshold) {
                db[collection].findOne({ _id: email }, (err, doc) => {
                    if (err) {
                        console.log('ERROR: Failed to look for email address in ' + collection);
                        sendResponse(res, 500, 'Server Error');
                    } else {
                        db[collection].save({ _id: email, threshold }, err => {
                            err && console.log('ERROR: Email address not saved to ' + collection);
                            sendResponse(res, err ? 500 : 200, err ? 'Server Error' : 'Subscribed');
                            const subject = (doc !== null && doc.threshold !== threshold) ? 'Hot on HN: Changed Notification Threshold' : 'Subscribed to Hot on HN';
                            email !== from && sendSubEmail({ email, subject, threshold });
                        });
                    }
                });
            } else {
                sendResponse(res, 400, 'Invalid Threshold');
            }
        } else if (action === 'unsub') {
            console.log('Unsubscribe request from ' + email);
            db[collection].findOne({ _id: email }, (err, doc) => {
                if (err) {
                    console.log('ERROR: Failed to look for email address in ' + collection);
                    sendResponse(res, 500, 'Server Error');
                } else if (!doc) {
                    console.log('Email address not found in ' + collection);
                    sendResponse(res, 404, 'Not Subscribed');
                } else {
                    db[collection].remove({ _id: email }, true, err => {
                        err && console.log('ERROR: Threshold for email address not updated in ' + collection);
                        sendResponse(res, err ? 500 : 200, err ? 'Server Error' : 'Unsubscribed');
                        sendUnsubEmail({ email, threshold: doc.threshold });
                    });
                }
            });
        } else {
           console.log('Bad request from ' + getClientIP(req));
           sendResponse(res, 400);
        }
    } else {
        console.log('Ignoring bad request from ' + getClientIP(req));
    }
});


server.listen(port, 'localhost');
console.log(divider);
console.log('Listening on localhost:' + port);
