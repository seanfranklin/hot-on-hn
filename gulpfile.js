const gulp = require('gulp'),
    concat = require('gulp-concat'),
sourcemaps = require('gulp-sourcemaps'),
    terser = require('gulp-terser');

gulp.task('scripts', function () {
    const srcFiles = [
        'src/scripts/el.js',
        'src/scripts/hot.js'
    ];
    return gulp.src(srcFiles)
        .pipe(sourcemaps.init())
        .pipe(terser())
        .pipe(concat('hot.min.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('www/'));
});
