/* globals el */
'use strict';

const hot = {

    opt: {},  // Holds user options from localStorage

    subMsgs: {
        change:  'Please enter a new threshold from 1000 to 6000',
        offline: 'You cannot subscribe while you are offline.',
        prev:    'If you are already subscribed your threshold will be updated.',
    },


    setOption(optName, defaultVal = null) {
        //console.log(`setOption(${optName}, ${defaultVal})`);
        hot.opt[optName] = (localStorage && localStorage[optName]) ? JSON.parse(localStorage[optName]) : defaultVal;
    },


    storeOption(optName, value) {
        //console.log(`storeOption(${optName}, ${value})`);
        hot.opt[optName] = value;
        localStorage && (localStorage[optName] = value);
    },


    setFooterFixed(isFixed = document.body.clientHeight + el.header.clientHeight <
            window.innerHeight - el.footer.clientHeight) {
        //console.log(`setFooterFixed(${isFixed})`);
        el.footer.classList[isFixed ? 'add' : 'remove']('fixed');
    },


    setThreshold() {
        //console.log('setThreshold()');
        el.threshold.innerHTML = hot.opt.threshold;
        const stories = document.querySelectorAll('[id].story');
        for (let i = 0; i < stories.length; i++) {
            const scoreNode = stories[i].childNodes[1],
                score = scoreNode ? parseInt(scoreNode.innerHTML) : undefined,
                isBelowThreshold = !isNaN(score) && score < hot.opt.threshold;
            stories[i].classList[isBelowThreshold ? 'add' : 'remove']('hidden');
        }
    },


    changeThreshold() {
        //console.log('changeThreshold()');
        const threshold = (hot.opt.threshold === 1000) ? 300 : hot.opt.threshold + 100;
        hot.storeOption('threshold', threshold);
        hot.setThreshold();
        hot.setFooterFixed();
    },


    setCards() {
        //console.log('setCards()');
        const isCards = hot.opt.isCards || (hot.opt.isCards === null && window.innerWidth < 800);
        el.cards.innerHTML = isCards ? 'Cards' : 'Max';
        document.body.classList[isCards ? 'add' : 'remove']('cards');
    },


    toggleCards() {
        //console.log('toggleCards()');
        const isCards = !document.body.classList.contains('cards');
        hot.storeOption('isCards', isCards);
        hot.setCards();
        hot.setFooterFixed();
    },


    requestNotify() {
        //console.log('requestNotify()');
        Notification.requestPermission().then(permission => {
            const isNotify = (permission === 'granted');
            hot.storeOption('isNotify', isNotify);
            el.notify.classList[isNotify ? 'add' : 'remove']('on');
        });
    },


    toggleNotify() {
        //console.log('toggleNotify()');
        if (hot.opt.isNotify) {
            hot.storeOption('isNotify', false);
            el.notify.classList.remove('on');
        } else if (!hot.opt.isNotify && Notification.permission === 'granted') {
            hot.storeOption('isNotify', true);
            el.notify.classList.add('on');
        } else {
            hot.requestNotify();
        }
    },


    toggleSubCard() {
        //console.log('toggleSubCard()');
        const pathname = document.body.classList.toggle('show-sub') ? '/subscribe/' : '/';
        history.replaceState(null, '', pathname);
        if (pathname === '/subscribe/') {
            el[hot.isValidEmail(el.subEmail.value) ? 'subThresh' : 'subEmail'].focus();
        } else {
            setTimeout(() => {
                if (el.subscribe.innerText === 'Subscribed ✓') {
                    el.subscribe.innerText = 'Change Threshold';
                }
            }, 900);
        }
    },


    openComments(storyId) {
        //console.log(`openComments(${storyId})`);
        window.open('https://news.ycombinator.com/item?id=' + storyId);
        return false;
    },


    hideDuplicates() {
        //console.log('hideDuplicates()');
        const stories = document.querySelectorAll('[id].story');
        for (let i = 0, ids = []; i < stories.length; i++) {
            const id = stories[i].id;
            id && ids.includes(id) && (stories[i].classList.add('duplicate'));
            ids.push(id);
        }
    },


    /**
     * Checks if the passed string is a valid Unix timestamp.
     * @param {number} timestamp
     * @return {boolean} true if url is valid, otherwise false
     */

    isTimestamp(timestamp) {
        //console.log(`hot.isTimestamp(${timestamp})`);
        return (typeof timestamp === 'number' &&
            !isNaN(timestamp) && timestamp > 1000000000);
    },


    /**
     * Checks if the passed timestamp represents today.
     * @param {number} [timestamp]
     * @return {boolean} true if today, otherwise false
     */

    isToday(timestamp = Date.now()) {
        //console.log(`isToday(${timestamp})`);
        const date = new Date(timestamp).toDateString(),
            today = new Date().toDateString();
        return date === today;
    },


    /**
     * Checks if the passed string is a valid email address.
     * @param {string} email
     * @return {boolean} true if email is valid, otherwise false
     */

    isValidEmail(email = el.subEmail.value) {
        //console.log(`isValidEmail(${email})`);
        const regex = /^(([^<>()\[\]\\.,;:\s@\"]+(\.[^<>()\[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
        return (email && regex.test(email)) ? true : false;
    },


    isValidSubThreshold(threshold = el.subThresh.value) {
        //console.log('isValidSubThreshold()');
        return threshold > 999 && threshold < 6001;
    },


    isValidSubForm() {
        //console.log('isValidSubForm()');
        return hot.isValidEmail(el.subEmail.value) && hot.isValidSubThreshold(el.subThresh.value);
    },


    /**
     * Checks if the passed string is a valid browser url.
     * @param {string} url
     * @return {boolean} true if url is valid, otherwise false
     */

    isValidUrl(url) {
        //console.log(`isValidUrl(${url})`);
        const pattern = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;
        return typeof url === 'string' && !!url.match(pattern);
    },


    /**
     * Checks the passed object has valid and necessary data.
     * This checks the keys specified by the Hacker News API v0.
     * @param {object} story
     * @return {boolean} true if data is valid, otherwise false
     */

    isValidStory(story) {
        //console.log(`isValidStory(${story})`);
        return (typeof story.id === 'number' && !isNaN(story.id) &&
            typeof story.score === 'number' && !isNaN(story.score) &&
            typeof story.title === 'string' && story.title.length &&
            typeof story.host === 'string' && story.host.length &&
            typeof story.comments === 'number' && !isNaN(story.comments) &&
            hot.isTimestamp(story.time) && hot.isValidUrl(story.url));
    },


    /**
     * Formats the passed date as month and day (e.g. 'Jan 1').
     * @param {date|number} [date] - Date object or Unix timestamp
     * @return {string} Formatted date
     */

    formatDate(date = new Date()) {
        //console.log(`formatDate(${date})`);
        hot.isTimestamp(date) && (date = new Date(date));
        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return months[date.getMonth()] + ' ' + date.getDate();
    },


    /**
     * Formats the passed date as 24 hour time (e.g. '12:00').
     * @param {date|number} [date] - Date object or Unix timestamp
     * @param {boolean} [is12Hr] - Use 12 hour time format
     * @return {string} Formatted time
     */

    formatTime(date = new Date(), is12Hr = true) {
        //console.log(`formatTime(${date}, ${is12Hr})`);
        hot.isTimestamp(date) && (date = new Date(date));
        let hours = date.getHours(),
            meridiem = '';
        const minutes = ('0' + date.getMinutes()).slice(-2);
        if (is12Hr) {
            meridiem = hours >= 12 ? ' pm' : ' am',
            hours = hours % 12;
            hours = hours ? hours : 12;
        }
        return `${hours}:${minutes}${meridiem}`;
    },


    formatStoryDates(className = 'date') {
        //console.log(`formatStoryDates(${className})`);
        const dateNodes = document.querySelectorAll(`.${className}`);
        for (let i = 0; i < dateNodes.length; i++) {
            const timestamp = dateNodes[i].dataset ? parseInt(dateNodes[i].dataset.timestamp) : null;
            if (hot.isTimestamp(timestamp)) {
                const isToday = hot.isToday(timestamp),
                    dateOrTime = isToday ? hot.formatTime(timestamp) : hot.formatDate(timestamp),
                    isDifferent = dateOrTime !== dateNodes[i].innerHTML;
                dateNodes[i].classList[isToday ? 'add' : 'remove']('today');
                isDifferent && (dateNodes[i].innerHTML = dateOrTime);
            }
        }
    },


    updateFooterTime(timestamp = Date.now()) {
        //console.log(`updateFooterTime(${timestamp})`);
        const time = hot.formatTime(timestamp);
        time && (el.updated.innerHTML = `Updated at ${time}`);
    },


    /**
     * Fetches a resource via an HTTP GET request.
     * @param {string} url
     * @param {function} success - Callback to run if request is successful
     * @param {function} fail - Callback to run if request fails
     */

    fetch(url, success, fail) {
        //console.log(`fetch(${url})`);
        const req = new XMLHttpRequest();
        req.onreadystatechange = () => {
            if (req.readyState === 4) {
                if (typeof success === 'function' && req.status === 200 &&
                    typeof req.responseText === 'string') {
                    success(req.responseText);
                } else if (typeof fail === 'function') {
                    fail(req.status);
                }
            }
        };
        req.open('GET', url, true);
        req.send();
    },


    getMoreStories(i) {
        //console.log(`getMoreStories(${i})`);
        const success = html => {
            el.stories.insertAdjacentHTML('beforeend', html);
            document.querySelectorAll('.new').length && hot.hideDuplicates();
            hot.setThreshold();
            el.more.innerHTML = 'more...';
        };
        const fail = status => {
            el.more.classList.add('disabled');
            el.more.innerHTML = (status === 404) ?
                'No more stories' : 'Error fetching more stories';
        };
        return () => {
            el.more.innerHTML = 'wait...';
            hot.fetch(`more${i++}.html`, success, fail);
        };
    },


    /**
     * Creates an HTML story item.
     * @param {object} story
     * @return {string} HTML
     */

    createHtml(story, isNew) {
        //console.log(`createHtml(${story}, ${isNew})`);
        const { id, score, time, title, url, host, comments } = story,
            isToday = hot.isToday(time),
            dateOrTime = isToday ? hot.formatTime(time) : hot.formatDate(time),
            hostClasses = story.host.length >= 30 ? 'host long' : 'host',
            dateClasses = isToday ? 'date today' : 'date';
        let storyClasses = 'story';
        isNew && (storyClasses += ' new');
        story.score < hot.opt.threshold && (storyClasses += ' hidden');
        return `
<a class="${storyClasses}" id="${id}" href="${url}" rel="noopener" target="_blank">
    <span class="score">${score}</span>
    <span class="title">${title}</span>
    <div class="meta">
        <span class="${hostClasses}">
            <img src="https://www.google.com/s2/favicons?domain=${host}">${host}
        </span>
        <span class="comments" onclick="return hot.openComments('${id}')">${comments}</span>
        <span class="${dateClasses}" data-timestamp="${time}">${dateOrTime}</span>
    </div>
</a>`;
    },


    notify(story) {
        //console.log(`notify(${story.title})`);
        const options = { body: story.title, icon: '/images/icon.png' },
            notification = new Notification('Hot on HN', options);
        notification.onclick = () => { window.open(story.url); };
        setTimeout(notification.close.bind(notification), 5000);
    },


    refreshStories(stories) {
        //console.log('refreshStories()');
        if (Array.isArray(stories) && stories.length) {
            let html = '';
            stories.filter(hot.isValidStory).forEach(story => {
                html += hot.createHtml(story);
            });
            html && (el.stories.innerHTML = html);
        }
    },


    updateStory(story, el = document.getElementById(story.id)) {
        //console.log(`updateStory(${story.id})`);
        el.classList[story.score < hot.opt.threshold ? 'add' : 'remove']('hidden');
        const props = ['score', 'title', 'comments'];
        props.forEach(prop => {
            const elem = el.querySelector('.' + prop);
            elem.innerHTML != story[prop] && (elem.innerHTML = story[prop]);
        });
        // TODO: Also update host and url if they have changed
    },


    updateStories(stories) {
        console.log('updateStories()');
        if (Array.isArray(stories) && stories.length) {
            stories.filter(hot.isValidStory).reverse().forEach(story => {
                const el = document.getElementById(story.id);
                if (el) {
                    hot.updateStory(story, el);
                } else {
                    document.querySelector('.story').insertAdjacentHTML('beforebegin', hot.createHtml(story, true));
                    hot.opt.isNotify && hot.notify(story);
                }
            });
        }
    },


    fetchStories() {
        //console.log('fetchStories()');
        hot.fetch('/stories.json', json => {
            hot.updateStories(JSON.parse(json));
        });
    },


    update() {
        //console.log('update()');
        navigator.onLine && hot.fetchStories();
        hot.formatStoryDates('today');
        navigator.onLine && hot.updateFooterTime();
    },


    subscribe(email = el.subEmail.value, threshold = el.subThresh.value) {
        //console.log(`subscribe(${email}, ${threshold})`);
        hot.fetch(`/sub/?email=${email}&threshold=${threshold}`, () => {
            hot.storeOption('subThresh', threshold);
            el.subscribe.innerText = 'Subscribed ✓';
            el.subMsg.innerText = `You'll receive an email notification when a story gains ${threshold} upvotes.`;
        });
    }
};


document.body.classList.remove('no-js');
window.onresize = () => {
    hot.setCards();
    hot.setFooterFixed();
};

hot.setOption('threshold', 300);
hot.setOption('isCards');
hot.setOption('isNotify');

if (Notification && Notification.permission) {
    el.notify.classList[hot.opt.isNotify ? 'add' : 'remove']('on');
    el.notify.classList.remove('hidden');
}

if (location.search) {
    const params = {};
    location.search.substring(1).split('&').forEach(param => {
        const arr = param.split('=');
        params[arr[0]] = arr[1];
    });
    const isValidEmail = hot.isValidEmail(params.email);
    isValidEmail && (el.subEmail.value = params.email);
    if (hot.isValidSubThreshold(params.threshold)) {
        el.subThresh.value = params.threshold;
        hot.storeOption('subThresh', params.threshold);
    }
    if (params.change && isValidEmail) {
        el.subscribe.innerText = 'Change Threshold';
        el.subMsg.innerText = hot.subMsgs.change;
    }
    history.replaceState(null, '', location.href.split('?')[0]);
} else {
    hot.setOption('subThresh', 1000);
}

if (location.pathname.substring(1, 10) === 'subscribe') {
    el[el.subEmail.value ? 'subThresh' : 'subEmail'].focus();
    document.body.classList.add('show-sub');
} else if (location.pathname !== '/') {
    history.replaceState(null, '', '/');
}

hot.setThreshold();
hot.setCards();
hot.setFooterFixed();
hot.formatStoryDates();
hot.updateFooterTime(parseInt(el.updated.dataset.timestamp));
hot.more = hot.getMoreStories(1);
el.subThresh.value = hot.opt.subThresh;

el.threshold.addEventListener('click', hot.changeThreshold);
el.cards.addEventListener('click', hot.toggleCards);
el.notify.addEventListener('click', hot.toggleNotify);
el.sub.addEventListener('click', hot.toggleSubCard);
el.subClose.addEventListener('click', hot.toggleSubCard);
el.more.addEventListener('click', hot.more);

el.more.style.display = 'block';
el.subscribe.disabled = !navigator.onLine || !hot.isValidSubForm();

el.subscribe.addEventListener('click', () => {
    hot.subscribe();
});
el.subThresh.addEventListener('input', () => {
    if (el.subscribe.innerText !== 'Subscribe') {
        el.subscribe.innerText = 'Change Threshold';
    }
    el.subscribe.disabled = !navigator.onLine || !hot.isValidSubForm();
});
el.subEmail.addEventListener('input', () => {
    el.subscribe.innerText = 'Subscribe';
    if (navigator.onLine) {
        el.subMsg.innerText = hot.subMsgs.prev;
    } else {
        el.subMsg.innerText = hot.subMsgs.offline;
    }
    el.subscribe.disabled = !navigator.onLine || !hot.isValidSubForm();
});


setInterval(hot.update, 300000);  // 5 minutes
document.addEventListener('online', () => {
    el.subscribe.disabled = !navigator.onLine || !hot.isValidSubForm();
    if (el.subscribe.innerText === 'Subscribe') {
        el.subMsg.innerText = hot.subMsgs.prev;
    }
    hot.update();
});
document.addEventListener('offline', () => {
    el.subscribe.disabled = true;
    if (el.subscribe.innerText === 'Subscribe') {
        el.subMsg.innerText = hot.subMsgs.offline;
    }
});
document.addEventListener('visibilitychange', () => {
    console.info('Event: document visibilitychange');
    document.visibilityState === 'visible' && hot.update();
});


if (sessionStorage && 'serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js').then(reg => {
        console.info('Service worker registered with scope:', reg.scope);
    }).catch(err => {
        console.error('Service worker not registered:', err);
    });

    navigator.serviceWorker.ready.then(() => {
        console.info('Service worker is ready');
        sessionStorage.clear();
        hot.isRefresh = true;
        hot.fetchStories = () => {
            navigator.onLine && hot.fetch('/stories.json');
        };
        hot.fetchStories();
    });

    navigator.serviceWorker.onmessage = event => {
        console.info('Event: message from service worker');
        const data = JSON.parse(event.data),
            isNew = sessionStorage.currentETag !== data.eTag;
        if (isNew && Array.isArray(data.stories)) {
            console.info('Message contains data from new stories.json');
            sessionStorage.currentETag = data.eTag;
            if (hot.isRefresh) {
                hot.isRefresh = false;
                hot.refreshStories(data.stories);
            } else {
                hot.updateStories(data.stories);
            }
        } else {
            console.info('Message indicates no new story data');
        }
    };
}
