'use strict';

const el = {

    header:     document.getElementById('header'),
    threshold:  document.getElementById('threshold'),
    cards:      document.getElementById('cards'),
    sub:        document.getElementById('sub'),
    notify:     document.getElementById('notify'),
    subClose:   document.getElementById('sub-close'),
    subEmail:   document.getElementById('sub-email'),
    subThresh:  document.getElementById('sub-thresh'),
    subscribe:  document.getElementById('subscribe'),
    subMsg:     document.getElementById('sub-msg'),
    stories:    document.getElementById('stories'),
    more:       document.getElementById('more'),
    updated:    document.getElementById('updated'),
    footer:     document.getElementById('footer')

};
