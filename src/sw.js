'use strict';

const sw = {

    cacheName: 'hot_cache_v1',

    assets: [
        '/',
        'index.html',
        'style.css',
        'hot.min.js',
        'fonts/Helvetica.woff2',
        'images/icon.png',
        'images/comments.svg',
        'images/notify.svg',
        'images/sub.svg',
        'images/sub_2.svg',
        'stories.json'
    ],


    /**
     * Gets the requested asset from the cache and returns it.
     * In the case of no match the promise resolves with undefined.
     */

    getFromCache(req) {
        //console.log('sw.getFromCache()');
        return caches.open(sw.cacheName).then(cache => {
            return cache.match(req);
        });
    },


    /**
     * Sends a message to the clients to tell them about the update.
     * The clients can use the ETag to check if the content has changed.
     * The ETag header usually contains a hash of the resource.
     */

    refreshClients(res) {
        //console.log('sw.refreshClients()');
        const eTag = res.headers.get('ETag');
        return res.json().then(stories => {
            const msg = JSON.stringify({ eTag, stories });
            return self.clients.matchAll().then(clients => {
                clients.forEach(client => {
                    client.postMessage(msg);
                });
            });
        });
    },


    /**
     * Updates the cache with a newer asset contained in the server response.
     */

    updateCache(req) {
        //console.log('sw.updateCache()');
        return caches.open(sw.cacheName).then(cache => {
            return fetch(req).then(res => {
                return cache.put(req, res.clone()).then(() => {
                    return res;
                });
            });
        });
    }

};


/**
 * Cache some assets upon service worker installation.
 * The waitUntil method ensures the worker is not considered
 * installed until all the cache is populated with all assets.
 */

self.addEventListener('install', ev => {
    console.info('Event: install service worker');
    self.skipWaiting();
    ev.waitUntil(caches.open(sw.cacheName).then(cache => cache.addAll(sw.assets)));
});


/**
 * Delete old caches when a new service worker is activated.
 */

self.addEventListener('activate', ev => {
    console.info('Event: activate service worker');
    ev.waitUntil(caches.keys().then(keys => {
        return Promise.all(keys.map(key => {
            if (key !== sw.cacheName) {
                console.info('Deleting old cache: ' + key);
                return caches.delete(key);
            }
        }));
    }).then(self.clients.claim()));
});


/**
 * Respond immediately with cached asset then update cache with asset from server.
 * If asset is stories.json then inform the clients after the cache is updated.
 */

self.addEventListener('fetch', ev => {
    const req = ev.request,
        asset = req.url.replace('https://hotonhn.com/', '');
    console.info('Event: fetch /' + asset);
    if (asset === 'stories.json') {
        ev.waitUntil(sw.update(req).then(sw.refreshClients, () => {
            ev.respondWith(sw.getFromCache(req));
        }));
    } else if (!asset || sw.assets.includes(asset)) {
        console.info('Requested asset should be cached');
        ev.respondWith(sw.getFromCache(req));
        ev.waitUntil(sw.updateCache(req));
    } else {
        console.info('Requested asset not cached');
        ev.respondWith(fetch(req));
    }
});
